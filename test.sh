#!/usr/bin/env bash
####
# Verify that the `sl` command actually works. Uses 
# tmux to run `sl -F`, and capture the output.
# Output is checked for a known piece of the train
####
##
## This is admittedly a very terrible way to do testing.
## Bonus points for demonstrating a better way to perform
##   some sort of test to make sure the command works by
##   replacing this script with something more test-like
####

set -e
set -o pipefail

tmux -u new-session -d -x 325 -y 30 -s sl-testing
tmux send -t sl-testing "./sl -F" ENTER
sleep 5
RES=$(tmux capture-pane -p)
tmux kill-session -t sl-testing

check="$(echo $RES | grep 'O=====O=====O=====O')"

if [ ! -z "$check" ]; then
    echo "Test Success"
    exit 0
fi
echo "Test Failure"
echo "$RES"
exit 1