# SL

Modifications have been made from the original repo to do the following:

 - A Build Shortcut in the Makefile (for future additions)
 - A Test Step, which will verify the sl command works
 - Definition for Production / Development. Controlled via `IS_PRODUCTION=true|false` on the make command
 - Versioning. Version specified in VERSION.md will be added to the sl.1 file on build

## Dependencies

### Usage
 - ncurses
 
### Development / Building
 - ncurses development libraries

### Testing
 - `tmux`

## Building

`make IS_PRODUCTION=(true|false) build`

# ORIGINAL README

SL(1): Cure your bad habit of mistyping
=======================================

SL (Steam Locomotive) runs across your terminal when you type "sl" as
you meant to type "ls". It's just a joke command, and not useful at
all.

Copyright 1993,1998,2014 Toyoda Masashi (mtoyoda@acm.org)
