#==========================================
#    Makefile: makefile for sl 5.1
#	Copyright 1993, 1998, 2014
#                 Toyoda Masashi
#		  (mtoyoda@acm.org)
#	Last Modified: 2014/03/31
#==========================================

.PHONY: build test clean

CC=gcc
CFLAGS=-O

ifdef IS_PRODUCTION
ifeq ($(IS_PRODUCTION),true)
CFLAGS=-O -D IS_PRODUCTION=true
endif
endif

CURRENT_VERSION := $(shell grep '\.SH VERSION' -A 1 sl.1 | tail -n 1)
NEW_VERSION := $(shell cat VERSION.md)

sl: sl.c sl.h
	$(CC) $(CFLAGS) -o sl sl.c -lncurses

build: sl
	sed -ri "s/^$(CURRENT_VERSION)/$(NEW_VERSION)/" ./sl.1

test:
	./test.sh

clean:
	rm -rf ./sl